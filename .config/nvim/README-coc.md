## Install CoC

Make sure node is installed
$ yay nvm
$ . /usr/share/nvm/init-nvm.sh
$ nvm install --lts
$ nvm use --lts


https://github.com/neoclide/coc.nvim

:CocInstall [coc pyright](coc-pyright.md)



