#!/usr/bin/env python
import sys
import datetime

week_days = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]
template = """# {date}  --  {week_day}

## Preguntas fundamentales

#### ¿Qué he hecho mal?
- 

#### ¿Qué he hecho bien?
- 

#### ¿Qué podría haber hecho diferente/mejor?
- 


## Lista de verificación

- [ ] Meditación
- [ ] Memento mori 
- [ ] Ducha fría
- [ ] Comida sana
- [ ] Ignorar el alcohol 
- [ ] Cuidado dental
- [ ] Deporte
- [ ] Caminar 5km 


## Notas

"""

date = datetime.date.today()
print(template.format(date=date, week_day=week_days[date.weekday()]))
