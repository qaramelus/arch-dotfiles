" Airline
let g:airline_section_warning = ''
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1 " Uncomment to display buffer tabline above

" let g:airline_section_z = ' %{strftime("%-I:%M %p")}'
" let g:airline_section_z = airline#section#create(['windowswap', 'obsession', '%3p%%'.g:airline_symbols.space, 'linenr', 'maxlinenr', g:airline_symbols.space.':%3V'])

let g:airline_section_z = airline#section#create(['%3p%%'.g:airline_symbols.space, 'linenr', 'maxlinenr', 'colnr', " \u27a2  %{strftime('%-I:%M %p')}"])
let g:airline_symbols.colnr = "\u2999"
let g:airline_symbols.linenr = ''

