""" qaramelus vim's inbbbbbt

" " Automatic reloading of .vimrc
autocmd! bufwritepost nvim source %


if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif


""" Vim-Plug
call plug#begin()

" Aesthetics - Main
Plug 'joshdick/onedark.vim'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'

" Widgets
Plug 'majutsushi/tagbar'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Editing ...
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
" Plug 'jiangmiao/auto-pairs'

" source control
Plug 'tpope/vim-fugitive'
Plug 'jreybert/vimagit'

" syntax and autocomplete
Plug 'sheerun/vim-polyglot'
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': { -> coc#util#install()}}
"Plug 'puremourning/vimspector'

"ODDDAZ
Plug 'scrooloose/nerdcommenter'
Plug 'vim-scripts/loremipsum'


Plug 'vimwiki/vimwiki'
Plug 'tbabej/taskwiki'
Plug 'plasticboy/vim-markdown'

call plug#end()


let g:vimspector_enable_mappings = 'HUMAN'

" BASICS
syntax on
filetype plugin indent on    " required

set nocompatible
set title
set termguicolors " Enable True Color Support (ensure you're using a 256-color enabled $TERM, e.g. xterm-256color)
set guicursor=a:blinkon100
set mouse=a
set encoding=utf-8
set clipboard+=unnamedplus

" set terminal and colorscheme
color onedark
set t_Co=256 
set tw=99   " width of document (used by gd)
set nowrap  " don't automatically wrap on load
set fo-=t   " don't automatically wrap text when typing
set formatoptions-=cro  " Stop newline continution of comments
set colorcolumn=120
highlight ColorColumn ctermbg=234

" 
set splitbelow splitright

" Numbers 
set hidden number    

" Comment out if slow
" set relativenumber cursorline

" Real programmers don't use TABs but spaces
set tabstop=4 softtabstop=4 shiftwidth=4 shiftround
set expandtab autoindent smartindent cindent

" Make search case insensitive
set hlsearch incsearch ignorecase smartcase

" Disable backup and swap files - they trigger too many events
set nobackup nowritebackup noswapfile

" Better copy & paste (for large blocks 'formatted' code)
set pastetoggle=<F2>


source $XDG_CONFIG_HOME/nvim/airline.vim
source $XDG_CONFIG_HOME/nvim/fzf.vim
source $XDG_CONFIG_HOME/nvim/nerdtree.vim
source $XDG_CONFIG_HOME/nvim/coc.vim

" vimwiki / taskwiki
let g:vimwiki_list = [{'path': '~/msync/vimwiki', 'syntax': 'markdown', 'ext': '.md'}, {'path': '~/.local/share/vimwiki/zoe', 'syntax': 'markdown', 'ext': '.md'}, ]
let g:vimwiki_listsyms = ' ○◐●✓'
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
let g:vimwiki_markdown_link_ext = 1
let g:taskwiki_markup_systax = 'markdown'
let g:taskwiki_data_location="~/msync/task"
let g:markdown_folding = 1
au BufNewFile ~/msync/vimwiki/diary/*.md execute 'silent 0r !~/.config/nvim/vimwiki-diary-template.py' | 6

" TagBar
let g:tagbar_width = 30

" Built-in plugins settings
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 22


" Some settings need to be set before the plugin loads
let g:black_virtualenv="~/tmp/blackvenv"
" let g:lightline = {'colorscheme': 'onedark'}


" " Disable cursor keys in normal mode.
" nnoremap <Left> <Nop>
" nnoremap <Right> <Nop>
" nnoremap <Up> <Nop>
" nnoremap <Down> <Nop>

" Removes highlight of your last search
noremap  <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>

" Use CTRL-S for saving, also in Insert mode
noremap <C-S> :update<CR>
vnoremap <C-S> <C-C>:update<CR>
inoremap <C-S> <C-O>:update<CR>

" Show buffers with F5
nnoremap <C-F5> :buffers<CR>:buffer<Space>

" " bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w + <movement>
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" 
" Replace all is aliased to S.
nnoremap S :%s//g<Left><Left>

" Save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" " easier moving of code blocks in visual mode. Does not leave visual mode
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation


" " Make cursor smaller on Insert mode
let &t_SI = "\<Esc>]50;CursorShape=2\x7" " Insert mode
let &t_SR = "\<Esc>]50;CursorShape=1\x7" " Replace mode
let &t_EI = "\<Esc>]50;CursorShape=0\x7"


" Specific file types
" au BufReadPost *.svelte set syntax=html
au BufNewFile,BufRead *.py  set foldmethod=indent
set foldlevel=3

"function! MoveAndFold(dir)
  "let pos = getpos('.')[1]
  "let nxt = l:pos + a:dir
  "let src = foldlevel(l:pos)
  "let dst = foldlevel(l:nxt)
"
  "if l:src < l:dst
    "exec l:nxt . "foldopen"
  "elseif l:src > l:dst
    "exec l:pos . "foldclose"
  "endif
  "exec ":" . l:nxt
"endfunction

"nnoremap j :call MoveAndFold(1)<cr>
"nnoremap k :call MoveAndFold(-1)<cr>


""" Custom Mappings
let mapleader = ","
nmap <leader>w :TagbarToggle<CR>
nmap \| <leader>w

" fzf
nmap <leader>s :Rg<CR>
nmap <leader>d :Files<CR>
nmap <leader>f :BLines<CR>

nmap <leader>r :so ~/.config/nvim/init.vim<CR>

  inoremap <nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1, 1)\<cr>" : "\<Right>"
  inoremap <nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0, 1)\<cr>" : "\<Left>"


" Theme overrides
hi Normal ctermbg=None guibg=None 
hi LineNr term=bold guibg=Grey20 guifg=LightSkyBlue3
hi LineNr term=bold guibg=Grey20 guifg=#ba8f18


hi TabLineFill guibg=Grey25
" hi TabLine guifg=Blue guibg=DarkGreen
hi TabLineSel term=bold guifg=#ba8f18 
