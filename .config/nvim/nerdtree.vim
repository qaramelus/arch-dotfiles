" NERDTree (start)

let g:NERDTreeShowHidden=0
let g:NERDTreeWinSize=40

" Start NERDTree. If a file is specified, move the cursor to its window.
"autocmd VimEnter * NERDTree | if argc() > 0 | wincmd p | endif

" Open the existing NERDTree on each new tab.
autocmd BufWinEnter * if getcmdwintype() == '' | silent NERDTreeMirror | endif

" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif
"
" Mappings 
nmap <leader>q :NERDTreeToggle<CR>
nmap \\ :NERDTreeFocus<CR> 

" NERDTree (end)
