docker ps >> /dev/null 2>&1|| sudo service docker start

export TZ="UTC"
export context="local"

command -v route.exe && export DISPLAY=$(route.exe print | grep 0.0.0.0 | head -1 | awk '{print $4}'):0.0 || true
