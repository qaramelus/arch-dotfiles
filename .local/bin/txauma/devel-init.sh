#!/bin/sh

export DATE=$(date +"%Y-%m-%d")

mkdir -p /var/tmp/tf
export TF_PLUGIN_CACHE_DIR=/var/tmp/tf 

export AWS_REGION=eu-central-1
export AWS_DEFAULT_REGION=eu-central-1

function pip_creds {
    export ARTIFACTORY_USER=`cat ~/.netrc | grep login | sed -e 's/login[[:blank:]]*//'`
    export ARTIFACTORY_PASSWORD=`cat ~/.netrc | grep password | sed -e 's/password[[:blank:]]*//'`
}

function aenv {
    if [ -f ./.venv/bin/activate ]
    then
        if [ "$(pwd)/.venv" != "$VIRTUAL_ENV" ]; then echo "Activating venv..." && source ./.venv/bin/activate; fi
    fi
}

function chpwd {
    aenv
}

function awsenv() {
   export AWS_PROFILE=$1
   echo "Current AWS profile: $AWS_PROFILE"
}

function clean_dir() {
    find -type d \
        -name ".pytest*" -or \
        -name "*.egg-info" -or \
        -name build -or \
        -name dist -or \
        -name .venv -or \
        -name venv -or \
        -name __pycache__ -or \
        -name .coverage -or \
        -name ".terraform*" -or \
        -name "*tfstate*" \
        | xargs -l rm -rf
}

[ ! -f ~/.netrc ] || pip_creds

# activate nvm
[ ! -f /usr/share/nvm/init-nvm.sh ] || . /usr/share/nvm/init-nvm.sh


source <(kubectl completion zsh)
