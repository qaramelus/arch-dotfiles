#!/bin/bash

docker network rm $(docker network ls | grep "bridge" | awk '/ / { print $1 }')
docker volume ls -qf dangling=true | xargs -r docker volume rm
docker images | grep none | awk '{print $3}' | xargs docker rmi -f
docker rm $(docker ps -qa --no-trunc --filter "status=exited")

